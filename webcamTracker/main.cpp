//
//  main.cpp
//  webcamTracker
//
//  Created by Junyu Teoh on 6/7/18.
//  Copyright © 2018 Junyu Teoh. All rights reserved.
//
#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

using namespace cv;
using namespace std;

int chooseDevice(){
    //user prompt here
    return 0;
}

void displayWebcam(int capID, int capMode){
    VideoCapture captureWebcam(capID); //new VideoCapture named CAP with capID int
    captureWebcam.set(CV_CAP_PROP_FRAME_WIDTH, 640); //set capture size instead of window size. further reducing cpu usage
    captureWebcam.set(CV_CAP_PROP_FRAME_HEIGHT,480); //same as above
    
    while(true){
        Mat webcamInput; //create new matrix named webcam
        captureWebcam.read(webcamInput);   //read captureWebcam onto matrix
        namedWindow("Debug Window", WINDOW_NORMAL); //new window with keepratio
        imshow("Debug Window", webcamInput); //show that window with webcam
        waitKey(capMode); //need to keep this as imshow needs an input to refresh the frame, and this is the only automated way (including the loop here). capMode is int to determine refresh rate. Lower number = higher refresh rate. Useful for controlling CPU usage. should bump up refresh rate when capturing photos
    }
}



int main() {
    displayWebcam(chooseDevice(),100); //call displaywebcam with choose device return int and captureMode
}
